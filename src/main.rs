use archetype::Cli;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    Cli::run().expect("Failed to run archetype.");

    Ok(())
}
