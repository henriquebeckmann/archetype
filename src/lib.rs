use clap::{Parser, Subcommand};
use std::io;

mod utils;

#[derive(Subcommand)]
enum Commands {
    New {
        #[arg(value_name = "PROJECT NAME")]
        name: String,
    },
    Init,
}

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,

    #[arg(short, long)]
    package_name: Option<String>
}

impl Cli {
    pub fn run() -> io::Result<()> {
        let cli = Cli::parse();

        match &cli.command {
            Some(Commands::New { name }) => println!("project name: {}", name),
            Some(Commands::Init) => println!("starting project in current dir"),
            None => eprintln!("type `archetype --help` to get help using archetype"),
        }

        Ok(())
    }
}
