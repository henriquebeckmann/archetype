use colored::Colorize;
use std::io;

/// Generic function to prompt for user input with an optional default value.
pub fn input<R, W>(
    msg: &str,
    default: Option<&str>,
    mut reader: R,
    mut writer: W,
) -> Result<String, &'static str>
where
    R: io::BufRead,
    W: io::Write,
{
    // Prepares and displays the prompt, including the default value if provided.
    let default_msg = default
        .map(|d| format!(" ({})", d))
        .unwrap_or_else(|| String::new());
    let format_msg = format!("{}{}: ", msg, default_msg.truecolor(63, 63, 70));

    // Writes the formatted prompt and ensures it's displayed immediately.
    write!(writer, "{}", format_msg).expect("Failed to write to output");
    writer.flush().expect("Failed to flush the buffer");

    // Reads the user input.
    let mut user_input = String::new();
    reader
        .read_line(&mut user_input)
        .expect("Failed to read the line");

    // Processes the user input, returning the default or provided input.
    let trimmed_input = user_input.trim();

    if trimmed_input.is_empty() {
        if default.is_none() {
            Err("Please enter a valid value.")
        } else {
            Ok(default.unwrap().to_string())
        }
    } else {
        Ok(trimmed_input.to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_input_with_valid_input() {
        // Arrange
        let msg = "Enter project name";
        let default = Some("my-app");
        let input_data = io::Cursor::new(b"archetype-app");
        let writer = Vec::new();

        // Act
        let input_value = input(msg, default, input_data, writer);

        // Assert
        assert_eq!(input_value, Ok(String::from("archetype-app")));
    }
    #[test]
    fn test_input_with_empty_input_and_no_default() {
        // Arrange
        let msg = "Enter project name";
        let default = None;
        let input_data = io::Cursor::new(b"\n");
        let writer = Vec::new();

        // Act
        let input_value = input(msg, default, input_data, writer);

        // Assert
        assert_eq!(input_value, Err("Please enter a valid value."));
    }

    #[test]
    fn test_input_with_empty_input_and_default() {
        // Arrange
        let msg = "Enter project name";
        let default = Some("my-app");
        let input_data = io::Cursor::new(b"\n");
        let writer = Vec::new();

        // Act
        let input_value = input(msg, default, input_data, writer);

        // Assert
        assert_eq!(input_value, Ok(String::from("my-app")));
    }
}
